package com.zoomonit.tools.jmxadmin.view;

import javax.management.MBeanInfo;

/**
 * Created by oaouattara on 20/05/16.
 */
public class JmxMBeanDescription {

    private String objectName;

    private MBeanInfo beanInfo;

    public JmxMBeanDescription withName(String canonicalName) {
        this.objectName = canonicalName;
        return this;
    }

    public JmxMBeanDescription withMBeanInfo(MBeanInfo beanInfo) {
        this.beanInfo = beanInfo;
        return this;
    }

    public String getObjectName() {
        return objectName;
    }

    public MBeanInfo getBeanInfo() {
        return beanInfo;
    }
}
