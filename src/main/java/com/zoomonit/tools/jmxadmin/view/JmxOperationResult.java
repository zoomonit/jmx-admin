package com.zoomonit.tools.jmxadmin.view;

import java.util.Map;

import javax.management.MBeanOperationInfo;

/**
 * Created by oaouattara on 23/05/16.
 */
public class JmxOperationResult {

    private MBeanOperationInfo operationInfo;
    private Map<String, Object> parameters;
    private Object result;

    public JmxOperationResult(MBeanOperationInfo operationInfo, Map<String, Object> parameters, Object result) {
        this.operationInfo = operationInfo;
        this.parameters = parameters;
        this.result = result;
    }

    public MBeanOperationInfo getOperationInfo() {
        return operationInfo;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public Object getResult() {
        return result;
    }
}
