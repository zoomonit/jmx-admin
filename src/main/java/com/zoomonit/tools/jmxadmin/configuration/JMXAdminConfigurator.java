package com.zoomonit.tools.jmxadmin.configuration;

public interface JMXAdminConfigurator {

    JMXAdminConfigurator activateLocalServer();

    JMXAdminConfigurator activateLocalServer(String serverAlias);

    JMXAdminConfigurator addServer(String serverAlias, String jmxUrl);

    JMXAdminConfigurator addServer(String serverAlias, String jmxUrl, String username, String password);

    JMXAdminConfigurator addServer(String serverAlias, String jmxUrl, boolean isSecured);

    JMXAdminConfigurator addServer(String serverAlias, String jmxUrl, String username, String password, boolean isSecured);

    JMXAdminConfigurator deactivateServer(String serverAlias);

    JMXAdminConfigurator includeObjectNames(String serverAlias, String ... objectNames);

    JMXAdminConfigurator excludeObjectNames(String serverAlias, String ... objectNames);
}
