package com.zoomonit.tools.jmxadmin.view;

import static java.lang.String.format;
import static org.apache.commons.lang.WordUtils.capitalize;
import static org.apache.commons.lang.WordUtils.capitalizeFully;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.zoomonit.tools.jmxadmin.JMXComponent;
import com.zoomonit.tools.jmxadmin.JmxPoolManager;


@RequestMapping("/jmxadmin")
@Controller
public class JMXRouterController {

    private static final Logger LOGGER = LoggerFactory.getLogger(JMXRouterController.class);

    @Autowired
    private JmxPoolManager jmxPoolManager;


    @RequestMapping(value = "/servers", method = RequestMethod.GET)
    @ResponseBody
    public List<JmxServer> getJmxServers() {
        LOGGER.debug("Get All Configured JMX Servers");
        ArrayList<JmxServer> servers = new ArrayList<>();
        for (JMXComponent jmxComponent : jmxPoolManager.getConfiguredComponents()) {
            JmxServer jmxServer = new JmxServer();
            jmxServer.setName(capitalize(jmxComponent.getName()));
            jmxServer.setUrl(jmxComponent.getUrl());
            servers.add(jmxServer);
        }
        return servers;
    }

    /**
     * Gives the list of all mbeans in component
     */
    @RequestMapping(value = "/{component}", method = RequestMethod.GET)
    @ResponseBody
    public JmxServerDescription getAllMBeanDescriptionOnComponentServer(@PathVariable String component) throws Exception {
        LOGGER.debug("Describing All JMX MBean on {}", component);

        MBeanServerConnection mBeanServerConnection = jmxPoolManager.getMBeanServerConnectionForComponent(component);

        return new JmxServerDescription()
                .withServerName(capitalizeFully(component))
                .withDomainsToMBeanName(generateServerDescription(mBeanServerConnection));
    }

    private SortedMap<String, List<String>> generateServerDescription(MBeanServerConnection mBeanServerConnection) throws IOException, MalformedObjectNameException {

        TreeMap<String,List<String>> domainsToMBeanName = new TreeMap<>();
        for (String domain : mBeanServerConnection.getDomains()) {
            ObjectName queryName = null;
            if ( domain != null )
            {
                queryName = new ObjectName( domain + ":*" );
            }
            Set<ObjectName> names = mBeanServerConnection.queryNames( queryName, null );
            List<String> results = new ArrayList<>(names.size());
            for ( ObjectName name : names )
            {
                results.add( name.getCanonicalName() );
            }
            Collections.sort( results );

            domainsToMBeanName.put(domain, results);
        }
        return domainsToMBeanName;
    }

    @RequestMapping(value = "/{component}/{objectName:.+}", method = RequestMethod.GET)
    @ResponseBody
    public JmxMBeanDescription getMBeanDescriptionOnComponentServer(@PathVariable String component,
                                                                  @PathVariable String objectName) throws Exception {
        LOGGER.debug("Describing JMX MBean on {} for Object {}", component, objectName);

        MBeanServerConnection mBeanServerConnection = jmxPoolManager.getMBeanServerConnectionForComponent(component);

        ObjectName name = new ObjectName(objectName);
        MBeanInfo beanInfo = mBeanServerConnection.getMBeanInfo(name);
        return new JmxMBeanDescription()
                .withName(name.getCanonicalName())
                .withMBeanInfo(beanInfo);

    }

    @RequestMapping(value = "/{component}/{objectName}/{operation}", method = {RequestMethod.POST , RequestMethod.PUT})
    @ResponseBody
    public JmxOperationResult callJMXMethodOnComponentServer(@PathVariable String component,
                                             @PathVariable String objectName,
                                             @PathVariable String operation,
                                             @RequestBody Map<String, Object> parameters) throws Exception {
        LOGGER.debug("Recieved JMX call on {} for Object {} and Method {}", component, objectName, operation);
        LOGGER.debug("List of parameter body: {}", parameters);

        MBeanServerConnection mBeanServerConnection = jmxPoolManager.getMBeanServerConnectionForComponent(component);

        ObjectName name = new ObjectName(objectName);
        MBeanInfo beanInfo = mBeanServerConnection.getMBeanInfo(name);

        // Looking for operation to invoke
        MBeanOperationInfo operationInfo = null;
        for ( MBeanOperationInfo info : beanInfo.getOperations() ) {
            if (StringUtils.equals(operation, info.getName())
                    && info.getSignature().length == parameters.size()) {
                LOGGER.debug("Candidate found for {} : {}", operation, info);
                // Compare the parameter names
                boolean goodCandidate = true;
                for (MBeanParameterInfo parameterInfo : info.getSignature()) {
                    if (!parameters.containsKey(parameterInfo.getName())){
                        LOGGER.debug("The candidate is wrong according to the parameter {}", parameterInfo.getName());
                        goodCandidate = false;
                        break;
                    }
                }
                if (goodCandidate) {
                    LOGGER.debug("Candidate validated.");
                    operationInfo = info;
                    break;
                }
            }
        }
        // If no matching operation is found, throw an exception
        if ( operationInfo == null ) {
            throw new IllegalArgumentException( "Operation " + operation
                    + " with " + parameters.size()
                    + " parameters doesn't exist in bean " + objectName );
        }

        // Now set parameters to invoke with
        Object[] params = new Object[parameters.size()];
        MBeanParameterInfo[] paramInfos = operationInfo.getSignature();
        String[] signatures = new String[paramInfos.length];
        for ( int i = 0; i < paramInfos.length; i++ ) {
            MBeanParameterInfo paramInfo = paramInfos[i];

            Object paramValue = parameters.get(paramInfo.getName());
            if ( paramValue == null ) {
                throw new IllegalArgumentException(paramInfo.getName()
                        + " not found in the request body while the operation requires it.");
            }
            params[i] = paramValue;
            signatures[i] = paramInfo.getType();
        }
        LOGGER.info("calling operation {} of mbean {} with params {}", operation, objectName, params);


        // Invoke operation, record execution time if measure flag is on
        Object result = mBeanServerConnection.invoke( name, operation, params, signatures );
        LOGGER.info("operation returns: {}", result);

        return new JmxOperationResult(operationInfo, parameters, result);
    }

    @RequestMapping(value = "/{component}/{objectName}/{attribute}", method = RequestMethod.GET)
    @ResponseBody
    public JmxAttribute getJMXInfoOnComponentServer(@PathVariable String component,
                                            @PathVariable String objectName,
                                            @PathVariable String attribute) throws Exception {
        LOGGER.debug("Recieved JMX call on {} for Object {} and Attribute {}", component, objectName, attribute);

        MBeanServerConnection mBeanServerConnection = jmxPoolManager.getMBeanServerConnectionForComponent(component);

        ObjectName name = new ObjectName(objectName);
        MBeanInfo beanInfo = mBeanServerConnection.getMBeanInfo(name);

        // Looking for operation to invoke
        MBeanAttributeInfo attributeInfo = null;
        for ( MBeanAttributeInfo info : beanInfo.getAttributes() ) {
            if (StringUtils.equals(attribute, info.getName())) {
                attributeInfo = info;
                break;
            }
        }

        // If no matching operation is found, throw an exception
        if ( attributeInfo == null ) {
            throw new IllegalArgumentException( "Attribute '" + attribute + "' doesn't exist in bean " + objectName );
        }

        Object attributeValue = mBeanServerConnection.getAttribute(name, attribute);

        return new JmxAttribute(attribute, attributeValue);
    }
}