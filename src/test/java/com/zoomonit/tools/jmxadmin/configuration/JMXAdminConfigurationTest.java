package com.zoomonit.tools.jmxadmin.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class JMXAdminConfigurationTest {

    @Test
    public void validateDefaultValues() {
        JMXAdminConfiguration jmxAdminConfiguration = new JMXAdminConfiguration();
        assertTrue(jmxAdminConfiguration.isActivateLocalServer());
        assertNotNull(jmxAdminConfiguration.getAliasToServerConfigurationMap());
        assertTrue(jmxAdminConfiguration.getAliasToServerConfigurationMap().isEmpty());
        assertNotNull(jmxAdminConfiguration.getPoolConfiguration());
    }

    @Test
    public void shouldNotFailOnDefaultConfiguration() throws JMXAdminConfigurationException {
        JMXAdminConfiguration jmxAdminConfiguration = new JMXAdminConfiguration();
        jmxAdminConfiguration.initialize();
        jmxAdminConfiguration.validate();
    }

    @Test
    public void shouldHaveOneJmxServerConfigurationForLocalServer() {
        JMXAdminConfiguration jmxAdminConfiguration = new JMXAdminConfiguration();
        jmxAdminConfiguration.initialize();
        assertEquals(1, jmxAdminConfiguration.getAliasToServerConfigurationMap().size());
    }
}