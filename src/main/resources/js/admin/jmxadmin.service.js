(function(){
    "use strict";

    angular.module("jmx-admin.admin").service("JMXAdminService", JMXAdminService);

    JMXAdminService.$inject = ["$http","$contextPath"];

    function JMXAdminService($http, $contextPath) {

        function getServers(successCallBack, errorCallBack) {
            return $http.get($contextPath + "/jmxadmin/servers", {})
                .then(function(response) {
                        console.log(response);
                        successCallBack(response.data);
                    }
                    , function(response) {
                        console.log(response);
                        errorCallBack(response);
                    });
        }

        function getServerDomains(serverPath, successCallBack, errorCallBack) {
            return $http.get($contextPath + "/jmxadmin/" + serverPath, {})
                .then(function(response) {
                        console.log(response);
                        successCallBack(response.data);
                    }
                    , function(response) {
                        console.log(response);
                        errorCallBack(response);
                    });
        }
        function getMBeanInfo(serverPath, mbean, successCallBack, errorCallBack) {
            return $http.get($contextPath + "/jmxadmin/" + serverPath + "/" + mbean, {})
                .then(function(response) {
                        console.log(response);
                        successCallBack(response.data);
                    }
                    , function(response) {
                        console.log(response);
                        errorCallBack(response);
                    });
        }

        function launchOperation(serverPath, mbean, operation, parameters, successCallback, errorCallback) {
            console.log(serverPath, mbean, operation, parameters);
            return $http.post($contextPath + "/jmxadmin/" + serverPath + "/" + mbean + "/" + operation, parameters).then(function(response){
                console.log(response);
                successCallback(response.data);
            }, function(response){
                console.log(response);
                errorCallback(response);
            });
        }

        return {
            getServers : getServers,
            getServerDomains: getServerDomains,
            getMBeanInfo : getMBeanInfo,
            launchOperation : launchOperation
        }
    }

})();