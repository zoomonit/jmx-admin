package com.zoomonit.tools.jmxadmin.configuration;

import javax.management.MBeanServerConnection;

/**
 * A Manager that will allow retrieval of a {@link MBeanServerConnection} to specific components.
 * This manager manages a pool of connectors using commons-pool. The parameters of the pools are :
 * <ul>
 * <li>setMaxTotal : {@link #DEFAULT_MAX_TOTAL}</li>
 * <li>setMinIdle : {@link #DEFAULT_MIN_IDLE}</li>
 * <li>setMaxIdle : {@link #DEFAULT_MAX_IDLE}</li>
 * </ul>
 */
public class JMXAdminPoolConfiguration {

    private static final int DEFAULT_MAX_TOTAL = 10;
    private static final int DEFAULT_MIN_IDLE = 0;
    private static final int DEFAULT_MAX_IDLE = 2;

    private int maxIdle = DEFAULT_MAX_IDLE;
    private int minIdle = DEFAULT_MIN_IDLE;
    private int maxTotal = DEFAULT_MAX_TOTAL;

    public int getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(int maxIdle) {
        this.maxIdle = maxIdle;
    }

    public int getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(int minIdle) {
        this.minIdle = minIdle;
    }

    public int getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(int maxTotal) {
        this.maxTotal = maxTotal;
    }
}
