package com.zoomonit.tools.jmxadmin.configuration;

import static org.apache.commons.lang.StringUtils.isBlank;

import java.util.ArrayList;
import java.util.List;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

public class JMXServerConfiguration {

    private boolean active = true;
    private String alias;
    private String jmxUrl;
    private String username;
    private String password;
    private boolean isSecured;

    private List<String> includedObjectNames = new ArrayList<>();
    private List<String> excludedObjectNames = new ArrayList<>();

    public String getAlias() {
        return alias;
    }

    public String getJmxUrl() {
        return jmxUrl;
    }

    public boolean isActive() {
        return active;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isSecured() {
        return isSecured;
    }

    public List<String> getIncludedObjectNames() {
        return includedObjectNames;
    }

    public List<String> getExcludedObjectNames() {
        return excludedObjectNames;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setJmxUrl(String jmxUrl) {
        this.jmxUrl = jmxUrl;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSecured(boolean secured) {
        isSecured = secured;
    }

    public void setIncludedObjectNames(List<String> includedObjectNames) {
        this.includedObjectNames = includedObjectNames;
    }

    public void setExcludedObjectNames(List<String> excludedObjectNames) {
        this.excludedObjectNames = excludedObjectNames;
    }

    void validate() throws JMXAdminConfigurationException {
        if (!active) {
            return;
        }
        boolean valid = true;
        StringBuilder errorSB = new StringBuilder("List of configuration issues:");
        if (isBlank(alias)) {
            valid = false;
            errorSB.append("\n\t- every server should have an alias");
        }
        if (isBlank(jmxUrl)) {
            valid = false;
            errorSB.append("\n\t- server '").append(alias).append("' should have a jmxurl");
        }
        for (String name : includedObjectNames) {
            try {
                new ObjectName(name);
            } catch (MalformedObjectNameException e) {
                valid = false;
                errorSB.append("\n\t- includeObjectName '").append(name).append("' wrong format: ").append(e.getMessage());
            }
        }
        for (String name : excludedObjectNames) {
            try {
                new ObjectName(name);
            } catch (MalformedObjectNameException e) {
                valid = false;
                errorSB.append("\n\t- excludeObjectName '").append(name).append("' wrong format: ").append(e.getMessage());
            }
        }
        if (!valid) {
            throw new JMXAdminConfigurationException(errorSB.toString());
        }

    }
}
