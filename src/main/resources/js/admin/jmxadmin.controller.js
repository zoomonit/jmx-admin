(function(){
    "use strict";

    angular.module("jmx-admin.admin").controller("JMXAdminController", JMXAdminController);

    JMXAdminController.$inject = ["JMXAdminService"];

    function JMXAdminController(JMXAdminService) {
        var vm = this;
        vm.servers = {};
        vm.mbeansMap = {};
        vm.detailedMbeans = {};
        vm.isLoading = {};
        vm.operationResults = {};
        vm.inputs = {};
        vm.collapsed = {};

        vm.getServers = getServers;
        vm.getDomains = getDomains;
        vm.toggleMBeanDetails = toggleMBeanDetails;
        vm.getMBeanDetailsState = getMBeanDetailsState;
        vm.launchOperation = launchOperation;
        vm.getInputType = getInputType;
        vm.toggleCollapse = toggleCollapse;
        vm.isCollapsed = isCollapsed;

        function getServers() {
            JMXAdminService.getServers(function(servers){
                $.each(servers, function (i, value) {
                    vm.servers[value.name] = value;
                    vm.servers[value.name].id = value.name.replace(/\s/g, "");
                });
            }, function(data){

                // FIXME Manage error.
            });

        }

        function getDomains(serverName) {
            JMXAdminService.getServerDomains(vm.servers[serverName].path, function(data){
                var domains = data.domainsToMbeanName;
                vm.servers[serverName].domains = [];
                $.each(domains, function (i, value) {
                    var domain = { name: i, mbeans: [] };
                    $.each(value, function(i, mbeanName) {
                        var mbean = {name: mbeanName};
                        domain.mbeans.push(mbean);
                        vm.mbeansMap[mbeanName] = mbean;
                    });
                    vm.servers[serverName].domains.push(domain);
                });
            }, function(data){
                // FIXME Manage error.
            });


        }

        function toggleMBeanDetails(serverName, mbeanName) {
            var uniqKey = serverName + mbeanName;
            if (vm.detailedMbeans[uniqKey]) {
                vm.detailedMbeans[uniqKey] = false;
            } else {
                if (uniqKey in vm.detailedMbeans) { // the mbean was already retrieved
                    vm.detailedMbeans[uniqKey] = true;
                } else {
                    vm.isLoading[uniqKey] = true;
                    JMXAdminService.getMBeanInfo(vm.servers[serverName].path, mbeanName, function (json) {
                        var mbeanInfo = json.beanInfo;
                        var mbean = vm.mbeansMap[mbeanName];
                        mbean["description"] = mbeanInfo.description;
                        mbean["operations"] = mbeanInfo.operations;
                        vm.isLoading[uniqKey] = false;
                        vm.detailedMbeans[uniqKey] = true;
                        console.log("OK " + uniqKey);
                    }, function(){
                        console.log("KO " + uniqKey );
                        vm.isLoading[uniqKey] = false;
                    });
                }
            }
        }

        function launchOperation(serverName, mbeanName, operation, parameters) {
            vm.operationResults = {};
            vm.isLoading[serverName + mbeanName + operation] = true;
            JMXAdminService.launchOperation(vm.servers[serverName].path, mbeanName, operation, parameters, function(data){
                vm.operationResults[serverName + mbeanName + operation] = JSON.stringify(data, null, 2);
                vm.inputs = {};
                vm.isLoading[serverName + mbeanName + operation] = false;
            }, function (data) {
                vm.inputs = {};
                vm.operationResults[serverName + mbeanName + operation] = data;
                vm.isLoading[serverName + mbeanName + operation] = false;
            });
        }

        function getMBeanDetailsState(serverName, mbeanName) {
            var uniqKey = serverName + mbeanName;
            return vm.detailedMbeans[uniqKey];
        }

        function getInputType(javatype) {
            switch(javatype) {
                case "int":
                case "java.lang.Integer":
                case "long":
                case "java.lang.Long":
                    return "number";
                case "java.lang.Boolean":
                    return "checkbox";
                default:
                    return "text";

            }
        }

        function toggleCollapse(id) {
            if ( id in vm.collapsed ) {
                vm.collapsed[id] = !vm.collapsed[id];
            } else {
                // First time we enter so the div collapsed. We open it
                vm.collapsed[id] = false;
            }
        }

        function isCollapsed(id) {
            if ( id in vm.collapsed ) {
                return vm.collapsed[id];
            }
            return true;
        }

    }

})();