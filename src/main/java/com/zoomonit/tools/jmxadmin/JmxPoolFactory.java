package com.zoomonit.tools.jmxadmin;

import java.io.IOException;
import java.util.Map;

import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class JmxPoolFactory extends BasePooledObjectFactory<JMXConnector> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JmxPoolFactory.class);

    private String urlString;
    private Map<String, ?> environment;

    public JmxPoolFactory(String url, Map<String, ?> environment) {
        this.urlString = url;
        this.environment = environment;
    }

    @Override
    public JMXConnector create() throws Exception
    {
        // code to create connection
        JMXServiceURL url = new JMXServiceURL(urlString);
        return JMXConnectorFactory.newJMXConnector(url, environment);
    }

    @Override
    public void activateObject(PooledObject<JMXConnector> jmxPooledObject) throws IOException {
        jmxPooledObject.getObject().connect();
    }

    @Override
    public PooledObject<JMXConnector> wrap(JMXConnector jmxConnector) {
        return new DefaultPooledObject<JMXConnector>(jmxConnector);
    }

    @Override
    public void destroyObject(PooledObject<JMXConnector> jmxPooledObject) throws IOException {
        // close the connection here
        LOGGER.trace("Closing the jmxConnector {}", jmxPooledObject );
        JMXConnector jMXConnector =jmxPooledObject.getObject();
        jMXConnector.close();
    }

    @Override
    public boolean validateObject(PooledObject<JMXConnector> jmxPooledObject) {
        LOGGER.trace("Validating the jmxConnector {}", jmxPooledObject );
        Boolean isValidConnection = Boolean.TRUE;
        try {
            jmxPooledObject.getObject().getMBeanServerConnection();
        }
        catch (IOException e) {
            LOGGER.trace("The connection is invalid", e);
            isValidConnection = Boolean.FALSE;
        }
        return isValidConnection;

    }
}