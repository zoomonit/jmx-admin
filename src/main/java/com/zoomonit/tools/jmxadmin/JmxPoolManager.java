package com.zoomonit.tools.jmxadmin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zoomonit.tools.jmxadmin.configuration.JMXAdminConfiguration;
import com.zoomonit.tools.jmxadmin.configuration.JMXAdminPoolConfiguration;
import com.zoomonit.tools.jmxadmin.configuration.JMXServerConfiguration;

/**
 * A Manager that will allow retrieval of a {@link MBeanServerConnection} to specific components.
 * This manager manages a pool of connectors using commons-pool. The parameters of the pools are :
 * You will be more interested in using the
 * {@link #getMBeanServerConnectionForComponent(String)} method that hides
 * the complexity of JMXConnector pool management.
 */
@Component
public class JmxPoolManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(JmxPoolManager.class);

    private GenericObjectPoolConfig config;

    Map<String, JmxPool> componentsToServerPools = new HashMap<String, JmxPool>();

    List<JMXComponent> configuredComponents = new ArrayList<>();

    @Autowired
    private JMXAdminConfiguration configuration;

    @PostConstruct
    public void initJmxPool() throws ConfigurationException {
        LOGGER.debug("JMX configuration values :\n\t{}", configuration);

        config = configurePool();

        configuration.getAliasToServerConfigurationMap().forEach((key, serverConfig) -> addComponent(serverConfig));

    }

    private GenericObjectPoolConfig configurePool() {
        GenericObjectPoolConfig config;
        config = new GenericObjectPoolConfig();
        JMXAdminPoolConfiguration poolConfiguration = configuration.getPoolConfiguration();
        config.setMaxIdle(poolConfiguration.getMaxIdle());
        config.setMinIdle(poolConfiguration.getMinIdle());
        config.setMaxTotal(poolConfiguration.getMaxTotal());
        config.setJmxEnabled(Boolean.TRUE);
        config.setTestOnBorrow(Boolean.TRUE);
        config.setTestOnReturn(Boolean.TRUE);
        config.setTestOnCreate(Boolean.TRUE);
        return config;
    }

    private void addComponent(JMXServerConfiguration serverConfig) {
        String alias = serverConfig.getAlias();
        if (serverConfig.isActive()) {
            LOGGER.info("add Component '{}' JMX config '{}'", alias, serverConfig);
            JmxPool jmxPool = new JmxPool(new JmxPoolFactory(serverConfig.getJmxUrl(), null), config);
            componentsToServerPools.put(alias, jmxPool);

            JMXComponent jmxComponent = new JMXComponent();
            jmxComponent.setName(alias);
            jmxComponent.setUrl(serverConfig.getJmxUrl());
            configuredComponents.add(jmxComponent);
        } else {
            LOGGER.info("{} JMX server deactivated by configuration", alias);
        }
    }

    public List<JMXComponent> getConfiguredComponents() {
        return configuredComponents;
    }

    /**
     * Gets a {@link MBeanServerConnection} to a specifed component.
     * We get a JMXConnector from the right JMXPool, then get the connection from it, and finally
     * return the connector to the pool.
     * @param component
     * @return A {@link MBeanServerConnection} to the specified component.
     * @throws Exception
     */
    public MBeanServerConnection getMBeanServerConnectionForComponent(String component) throws Exception {
        LOGGER.trace("Getting a MBeanServerConnection for {}", component);
        JMXConnector jmxConnector = getJMXConnectorForComponent(component);
        try {
            return jmxConnector.getMBeanServerConnection();
        } finally {
            returnJMXConnectorToPool(component, jmxConnector);
        }

    }

    /**
     * Gets a {@link JMXConnector} for the specified component, borrowing it from a pool of
     * connection. <br/>
     * It's important to not that the connector should be considered as borrowed only.
     * Upon usage completion of that connector, don't forget to return the connector to the pool
     * using the
     * {@link #returnJMXConnectorToPool(String, JMXConnector)} method
     * @param component
     * @return a {@link JMXConnector} from the connection pool of that component.
     * @throws Exception
     * @see #returnJMXConnectorToPool(String, JMXConnector)
     */
    public JMXConnector getJMXConnectorForComponent(String component) throws Exception {
        LOGGER.trace("Getting a JMX connector for {}", component);
        if (componentsToServerPools.containsKey(component)) {
            JmxPool jmxPool = componentsToServerPools.get(component);
            return jmxPool.borrowObject();
        } else {
            throw new IllegalArgumentException(component + " has no JMX connection pool configured!");
        }
    }

    /**
     * @param component
     * @param jmxConnector
     */
    public void returnJMXConnectorToPool(String component, JMXConnector jmxConnector) {
        LOGGER.trace("Returning a JMX connector for {}", component);
        if (componentsToServerPools.containsKey(component)) {
            JmxPool jmxPool = componentsToServerPools.get(component);
            jmxPool.returnObject(jmxConnector);
        } else {
            throw new IllegalArgumentException(component + " has no JMX connection pool configured!");
        }
    }

    @PreDestroy
    public void cleanup() {
        LOGGER.trace("Cleaning all the connection pool up.");
        for (JmxPool jmxPool : componentsToServerPools.values()) {
            jmxPool.close();
        }
    }

}
