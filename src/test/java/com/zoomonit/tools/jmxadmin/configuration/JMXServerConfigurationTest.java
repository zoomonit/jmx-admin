package com.zoomonit.tools.jmxadmin.configuration;

import static org.junit.Assert.*;

import org.junit.Test;

public class JMXServerConfigurationTest {

    @Test(expected = JMXAdminConfigurationException.class)
    public void shouldNotValidateServerConfiguration_EmptyConfiguration() throws JMXAdminConfigurationException {
        new JMXServerConfiguration().validate();
    }

    @Test
    public void shouldValidateServerConfiguration_EmptyConfigurationWhenServerInactive() throws JMXAdminConfigurationException {
        JMXServerConfiguration serverConfiguration = new JMXServerConfiguration();
        serverConfiguration.setActive(false);
        serverConfiguration.validate();
    }

    @Test(expected = JMXAdminConfigurationException.class)
    public void shouldNotValidateServerConfiguration_EmptyJMXURL() throws JMXAdminConfigurationException {
        JMXServerConfiguration serverConfiguration = new JMXServerConfiguration();
        serverConfiguration.setAlias("MyServer");
        serverConfiguration.validate();
    }

    @Test(expected = JMXAdminConfigurationException.class)
    public void shouldNotValidateServerConfiguration_EmptyAlias() throws JMXAdminConfigurationException {
        JMXServerConfiguration serverConfiguration = new JMXServerConfiguration();
        serverConfiguration.setJmxUrl("MyjmxURL");
        serverConfiguration.validate();
    }

    @Test(expected = JMXAdminConfigurationException.class)
    public void shouldNotValidateServerConfiguration_badIncludes() throws JMXAdminConfigurationException {
        JMXServerConfiguration serverConfiguration = new JMXServerConfiguration();
        serverConfiguration.setJmxUrl("MyjmxURL");
        serverConfiguration.setAlias("myAlias");
        serverConfiguration.getIncludedObjectNames().add("bad_include");
        serverConfiguration.validate();
    }

    @Test(expected = JMXAdminConfigurationException.class)
    public void shouldNotValidateServerConfiguration_badExcludes() throws JMXAdminConfigurationException {
        JMXServerConfiguration serverConfiguration = new JMXServerConfiguration();
        serverConfiguration.setJmxUrl("MyjmxURL");
        serverConfiguration.setAlias("myAlias");
        serverConfiguration.getExcludedObjectNames().add("bad_exclude");
        serverConfiguration.validate();
    }

    @Test
    public void shouldValidateServerConfiguration_noIncludesExcludes() throws JMXAdminConfigurationException {
        JMXServerConfiguration serverConfiguration = new JMXServerConfiguration();
        serverConfiguration.setJmxUrl("MyjmxURL");
        serverConfiguration.setAlias("myAlias");
        serverConfiguration.validate();
    }

    @Test
    public void checkTheDefaultValuesOfAConfiguration() throws JMXAdminConfigurationException {
        JMXServerConfiguration serverConfiguration = new JMXServerConfiguration();
        assertTrue(serverConfiguration.isActive());
        assertFalse(serverConfiguration.isSecured());
        assertNull(serverConfiguration.getJmxUrl());
        assertNull(serverConfiguration.getAlias());
        assertNull(serverConfiguration.getUsername());
        assertNull(serverConfiguration.getPassword());
        assertNotNull(serverConfiguration.getIncludedObjectNames());
        assertNotNull(serverConfiguration.getExcludedObjectNames());
        assertTrue(serverConfiguration.getExcludedObjectNames().isEmpty());
        assertTrue(serverConfiguration.getIncludedObjectNames().isEmpty());
    }
}