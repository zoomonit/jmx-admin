package com.zoomonit.tools.jmxadmin.configuration;

public class JMXAdminConfigurationException extends Exception {

    public JMXAdminConfigurationException(String message) {
        super(message);
    }

    public JMXAdminConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
